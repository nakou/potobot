# Potobot

Ahoy,

Potobot is a poor bastard of a prototype bot for Discord that create new voice channels on the fly when people go into a designated one.
Like, you have a category "Voice Channel", you name a channel "Open New Channel", and when people go into the chan, the bot create a new chan, give it a cool name and send them on their way... into the new chan (if it wasn't obvious).

## Commands 

- /potinfo : just a command to check if the bot is responding well to the good role.
- /cleanmess : clean empty channels that could be left after a bug, a bot brainfart or a crash.

## Installation

Just get the project, slap it into a nodejs server with NPM

Give it a little
```bash
npm install discord.js
```

And when computer stop blip bloup, you can launch the app.js and badaboom.

Wait no.

## Configuration.

First, you must open app.js and modify some vars to make it on your *smack lips* taste :
```js
const botToken = "not_today_satan";
```
botToken is the Discord token of your bot so feel free to fill it with the random shit Discord gives you.

```js
const authorizedRole = "Bossman";
```
authorizedRole is the role that will be NOT ignored by the bot when commands are typed.

```js
const voiceChanNameAutoCreateToken = "new_channel";
```
voiceChanNameAutoCreateToken is the name of the channel people will have to go into to generate new channels.

```js
const voiceChanCreatedToken = "(auto)";
```
voiceChanCreatedToken is a piece of word that will be added into your new auto-generated channel name. It allow the bot to know wich chans have to be deleted if there is nobody in it.
It's made like that so even if the bot crash, you'll still be able to simple clean all the zombie chans by just connecting and leaving them. The bot will catch and destroy them anyway.

```js
const arrName = ["Timmy","Tommy"];
```
arrName, this is the cool part. It's a list of names your chans can have. With the token it's gonna be "arrNameXvoiceChanCreatedToken". In my values, can be like "Tommy(auto)".

Don't forget to be original

## Contributing
Probably wont be updated, since it was just a put-together-in-2-hours prototype but fill free to make it evolve into a monster that will one day be the source of the human race demise.

## License
[MIT](https://choosealicense.com/licenses/mit/)