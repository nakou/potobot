const { Client, Intents, VoiceChannel, Message, Guild } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });

const botToken = "not_today_satan";
const authorizedRole = "Bossman";
const voiceChanNameAutoCreateToken = "new_channel";
const voiceChanCreatedToken = "(auto)";
const arrName = ["Jimmy","Timmy","Tommy"];

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
    if(msg.author.bot){
        return;
    }
    //console.log(`Message from ${msg.author.tag} : ${msg.content}`);
    if(!msg.member.roles.cache.some( role => role.name === authorizedRole)){
        console.log(`${msg.author.tag} is not ${authorizedRole}, ignoring`);
        return;
    }
    if(msg.content.charAt(0) == '/')
        checkCommand(msg);
});

client.on('voiceStateUpdate', (oldMember, newMember) => {
    let newUserChannel = newMember.channel;
    let oldUserChannel = oldMember.channel;
    if(newUserChannel != null){
        console.log(`Someone entered the channel ${newUserChannel.name} `);
        if(newUserChannel.name.includes(voiceChanNameAutoCreateToken)){
            var newChanName = getRandomName() + voiceChanCreatedToken;
            newUserChannel.guild.channels.create(newChanName, {type: 'voice', parent : newUserChannel.parent}).then((channel) => {
                newMember.setChannel(channel);
            });
        }
    }
    if(oldUserChannel != null) {
        console.log(`Someone left the channel ${oldUserChannel.name} `);
        checkIfShouldDestroyChannel(oldUserChannel);        
    }
})

function checkCommand(msg){
    console.log(`${msg.author.tag} is using the command ${msg.content}`);
    if(msg.content == "/potinfo")
    {
        msg.channel.send("I AM A BOT uwu");
        return;
    }
    if(msg.content == "/cleanmess")
    {
        cleanZombieChannels(msg);
        return;
    }
    console.log(`${msg.content} is not a known command, ignoring`);
}

function cleanZombieChannels(msg){
    var cleanedChannels = 0;
    msg.guild.channels.cache.forEach((channel) => {
        if(channel.type === 'voice')
            if(checkIfShouldDestroyChannel(channel))
                cleanedChannels++;
    });
    msg.channel.send("I have cleaned " + cleanedChannels + " empty channels. Am I a good bot?");
}

function checkIfShouldDestroyChannel(channel, cleaning = false){
    if(channel.name.includes(voiceChanCreatedToken) && !channel.members.first()){
        if(!cleaning)
            console.log(`the last member of channel ${channel.name} is gone`);
            channel.delete();
        return true;
    }
    return false;
}

function getRandomName(){
    var maxInt = arrName.length;
    return arrName[Math.floor(Math.random() * maxInt)];
}

client.login(botToken);